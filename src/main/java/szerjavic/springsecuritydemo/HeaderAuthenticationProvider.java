package szerjavic.springsecuritydemo;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

public class HeaderAuthenticationProvider implements AuthenticationProvider {

    private UserDetailsService userDetailsService;

    public HeaderAuthenticationProvider(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        String authValue = (String) authentication.getPrincipal();

        if (authValue == null) {
            return null;
        }

        return doAuthentication(authValue);

    }

    private Authentication doAuthentication(String authValue) {

        UserDetails user = userDetailsService.loadUserByUsername(authValue);

        HeaderAuthenticationToken authenticationToken = new HeaderAuthenticationToken(user);
        authenticationToken.setAuthenticated(true);

        return authenticationToken;

    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(HeaderAuthenticationToken.class);
    }
}
