package szerjavic.springsecuritydemo;

import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HeaderRestController {

    @PostMapping("/header/login")
    public Authentication headerLogin(Authentication auth) {
        return auth;
    }
}
