package szerjavic.springsecuritydemo;

import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HeaderAuthenticationFilter extends AbstractAuthenticationProcessingFilter {


    public static final String HEADER_LOGIN_URL = "/header/login";

    protected HeaderAuthenticationFilter() {
        super(new AntPathRequestMatcher(HEADER_LOGIN_URL, HttpMethod.POST.name()));
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        doFilter((HttpServletRequest) request, (HttpServletResponse) response, chain);
    }

    private void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (!requiresAuthentication(request, response)) {
            chain.doFilter(request, response);
        } else {
            try {
                Authentication authenticationResult = attemptAuthentication(request, response);
                if (authenticationResult == null) {
                    return;
                }

                successfulAuthentication(request, response, chain, authenticationResult);
            } catch (InternalAuthenticationServiceException var5) {
                logger.error("An internal error occurred while trying to authenticate the user.", var5);
                unsuccessfulAuthentication(request, response, var5);
            } catch (AuthenticationException var6) {
                unsuccessfulAuthentication(request, response, var6);
            }

        }
    }

    @Override
    protected boolean requiresAuthentication(HttpServletRequest request, HttpServletResponse response) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (super.requiresAuthentication(request, response) && authentication == null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws AuthenticationException, IOException, ServletException {

        String authValue = httpServletRequest.getHeader("X-AUTH-VALUE");

        HeaderAuthenticationToken attemptAuthToken = new HeaderAuthenticationToken(authValue);
        Authentication authentication = getAuthenticationManager().authenticate(attemptAuthToken);

        return authentication;
    }
}
