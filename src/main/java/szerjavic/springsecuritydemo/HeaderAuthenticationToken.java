package szerjavic.springsecuritydemo;

import org.springframework.security.authentication.AbstractAuthenticationToken;

public class HeaderAuthenticationToken extends AbstractAuthenticationToken {

    private static final long serialVersionUID = 656185069492252530L;
    private final Object principal;

    public HeaderAuthenticationToken(Object principal) {
        super(null);
        this.principal = principal;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return principal;
    }
}
